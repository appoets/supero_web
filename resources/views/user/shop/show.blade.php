<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{Setting::get('site_title')}}</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="{{ Setting::get('site_favicon', asset('favicon.ico')) }}">
    <!-- Bootstrap CSS -->
    <link href="{{ asset('assets/user/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Font Awesome CSS -->
    <link href="{{ asset('assets/user/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- Ionicons CSS -->
    <link href="{{ asset('assets/user/ionicons/css/ionicons.min.css')}}" rel="stylesheet">
    <!-- Material Icons CSS -->
    <link href="{{ asset('assets/user/material-icons/css/materialdesignicons.min.css')}}" rel="stylesheet">
    <!-- Slick CSS -->
    <link href="{{ asset('assets/user/css/slick.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/user/css/slick-theme.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/user/css/owl.carousel.css')}}" rel="stylesheet">
    <!-- Style CSS -->
    <link href="{{ asset('assets/user/css/style.css')}}" rel="stylesheet">

</head>

<body oncontextmenu="return false;" >
    <section class="top_header">
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-sm-7">
                  <div class="responsive_humble_menu">
                    <i class="fa fa-bars"></i>
                  </div>
                    <div class="location-col">
                        <span><i class="fa fa-map-marker" aria-hidden="true"></i></span> {{@$zipcode}} <span class="change_location"><button type="button"  class="loc-btn" data-toggle="modal" data-target="#schedule_modal">change location</button></span>
                    </div>
                    <div id="schedule_modal" class="modal fade schedule-modal" role="dialog">
                    <div class="modal-dialog">

                      <!-- Modal content-->
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Zipcode</h4>
                        </div>
                       
                        <div class="modal-body">
                          <form class="form-horizontal" role="form" method="GET" id="user_form"  action="{{ route('zip.store') }}" enctype="multipart/form-data">
                           {{ csrf_field() }} 
                            <input type="text" class="form-control" name="zipcode">
                            <input type="submit" value="Submit" name="">
                            
                    </form>
                        

                        </div>
                        <div class="modal-footer">
                         
                        </div>

                        
                      </div>

                    </div>
                  </div>
                    <div class="delevery_status">Earliest Delivery: All slots currently taken. Please check back later.</div>
                </div>
                 <div class="col-md-5 col-sm-5">
                   <div class="cart_sec mobile-vertion">
                        <button type="button" class="btn-info btn-lg" data-toggle="modal" data-target="#mycart"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i></button>
                    </div>
                  <div class="right_side_responsive_menu">
                      <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                  </div>
                     <div class="right_menu">

                        <ul>
                            <li>
                         <div class="store_name">
                             <div class="dropdown">
                              <button class=" btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Locate Our Stores <i class="fa fa-caret-down" aria-hidden="true"></i>
                              </button>

                              <ul class="dropdown-menu outletlist">
                              <li><a href="#">Austin Heights, JB</a></li>
                              <li><a href="#">Bangi Gateway</a></li>
                              <li><a href="#">Bangsar Market @ KLEC Mall</a></li>
                              <li><a href="#">Centrepoint Bandar Utama</a></li>
                              <li><a href="#">Da Men Mall USJ1</a></li>
                              <li><a href="#">Damansara Perdana</a></li>
                              <li><a href="#">DPulze Cyberjaya</a></li>
                              <li><a href="#">Eco Ardence</a></li>
                              <li><a href="#">Empire Shopping Gallery</a></li>
                             </ul>
                            </div>
                         </div>
                     </li>
                     <li><a href="#">Faq</a></li>
                     <li class="mycart_list">MyCart<span class="cart_count">@if(Auth::user()){{  count($Cart['carts']) }}@else  0  @endif</span><i class="fa fa-caret-down" aria-hidden="true"></i>
                        <div class="cart_list">
                          @if(Auth::user())
                              <div class="cart_head">
                                  <h3>You have {{  count($Cart['carts']) }} product(s) in your cart.</h3>
                              </div>

                              @forelse($Cart['carts'] as $item)
                              <div class="product_details">
                                  <table width="100%" cellspacing="10px" cellspacing="5px">
                                      <tr>
                                          <th width="40%">Item</th>
                                          <th width="20%">Quantity  </th>
                                          <th width="20%">Price (RM)</th>
                                          <th width="20%">Total (RM)</th>
                                      </tr>
                                      <tr>
                                        <?php 
                                          
                                                $url = url('restaurant/details?name=Walmart&myaddress=home');
                                            
                                        ?>
                                          <td width="40%">
                                              <span><img src=""></span>
                                              <a href="{{$url}}">{{@$item->product->name}}</a>
                                              <span>120g</span>
                                          </td>
                                          <td width="20%">{{@$item->quantity}}</td>
                                          <td width="20%">{{@$item->product->prices->orignal_price}}  </td>
                                          <td width="20%">{{@$item->product->prices->orignal_price}} <span><i class="fa fa-trash" aria-hidden="true"></i></span></td>
                                      </tr>
                                  </table>
                              </div>
                              @empty
                                    
                              @endforelse
                          @else
                          
                          @endif    
                               <!-- <div class="product_details">
                                  <table width="100%" cellspacing="10px" cellspacing="5px">
                                      <tr>
                                          <th width="40%">Item</th>
                                          <th width="20%">Quantity  </th>
                                          <th width="20%">Price (RM)</th>
                                          <th width="20%">Total (RM)</th>
                                      </tr>
                                      <tr>
                                          <td width="40%">
                                              <span><img src=""></span>
                                              <a href="#">The Laughing Cow Creamy and Milky Cheese Spread</a>
                                              <span>120g</span>
                                          </td>
                                          <td width="20%">2</td>
                                          <td width="20%">7.89  </td>
                                          <td width="20%">15.78 <span><i class="fa fa-trash" aria-hidden="true"></i></span></td>
                                      </tr>
                                  </table>
                              </div> -->
                        </div>
                     </li>
                     @if(Auth::guest())
                     <li><a href="javascript:void(0);" class="login-item signinform" >Login</a></li>
                     <li><a href="javascript:void(0);" class="signup-item signupform" >Register</a></li>
                     @endif
                     @if(Auth::user())
                     <li>
                        <a href="javascript:void(0);" onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                    @lang('menu.user.logout')</a>
                        </li>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                     @endif
                 </ul>
                     </div>
                 </div>
            </div>
        </div>
    </section>
    <section class="header_bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-4 logo">
                    <div class="logo-sec"><a href="{{url('/')}}"><img src=""></a></div>
                </div>
                <div class="col-md-7 col-sm-4 search">
                    <div class="search_sec">
                        <input type="text" name="search"><button type="button" class="search"><i class="fa fa-search" aria-hidden="true"></i></button>
                    </div>
                    <div class="top_search">
                        Top searches:
                        <ul>
                            <li><a href="#">Milk</a></li>
                            <li><a href="#">Almond Milk</a></li>
                            <li><a href="#">Cheese</a></li>
                            <li><a href="#">Chicken</a></li>
                            <li><a href="#">Bread</a></li>
                            <li><a href="#">Yogurt</a></li>
                            <li><a href="#">Milo</a></li>
                            <li><a href="#">Rice</a></li>
                            <li><a href="#">Coffee</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2 col-sm-4 cart-btn">
                    <div class="cart_sec">
                        <button type="button" class="btn-info btn-lg" data-toggle="modal" data-target="#mycart"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i> My Cart</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="landing_cnt">
        <div class="container">
            <div class="row ">
                <div class="col-md-3 col-sm-3">
                    <div class="side_nav">
                        <h2>Catagories</h2>
                        <ul>
                          @foreach($cat as $key=>$val)
                            
                            <li><span></span><a href="{{url('/catagory/products')}}?cat={{$val->id}}">{{ $val->name }}</a></li>
                            
                          @endforeach
                          <!-- <li><span></span><a href="#">Fresh Market</a></li>
                          <li class="sub_menu"><span></span><a href="#">Food Essentials <i class="fa fa-caret-right" aria-hidden="true"></i></a>
                              <ul class="sub_items">
                                <li><a href="#">Sunflower Oil</a></li>
                                <li><a href="#">Olive Oil</a></li>
                                <li><a href="#">Vegitable Oil</a></li>
                                <li><a href="#">Corn Oil</a></li>
                                <li><a href="#">Sesame Oil</a></li>
                                <li><a href="#">Canola Oil</a></li>
                              </ul>
                          </li>
                          <li><span></span><a href="#">Organics</a></li>
                          <li><span></span><a href="#">Alcohol</a></li>
                          <li><span></span><a href="#">bakery</a></li>
                          <li><span></span><a href="#">Baby Care</a></li>
                          <li><span></span><a href="#">Pet Shop</a></li>
                          <li><span></span><a href="#">Beverages</a></li>
                          <li><span></span><a href="#">Snacks</a></li>
                          <li><span></span><a href="#">Beauty Health</a></li>
                          <li><span></span><a href="#">Office Supplies</a></li>
                          <li><span></span><a href="#">Frozenfood</a></li>
 -->                        </ul>
                    </div>
                </div>
                <div class="col-md-9 col-sm-9">
                    <div class="banner_slide banner_sec"><div id="myCarousel" class="carousel slide" data-ride="carousel">
                      <!-- Indicators -->
                      <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>

                      </ol>

                      <!-- Wrapper for slides -->
                      <div class="carousel-inner">
                        <div class="item active">
                          <img src="https://wallpapercave.com/wp/wp3152222.jpg" alt="Los Angeles">
                        </div>

                        <div class="item">
                          <img src="https://wallpapercave.com/wp/wp3152222.jpg" alt="Chicago">
                        </div>


                      </div>

                      <!-- Left and right controls -->
                      <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                        <span class="sr-only">Previous</span>
                      </a>
                      <a class="right carousel-control" href="#myCarousel" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                        <span class="sr-only">Next</span>
                      </a>
                    </div>
                    </div>
                    @if(count($FeaturedProducts1) != 0)
                    <!--dynamic-->
                     <div class="product-items">
                      <h2></h2>
                      <div class="p_item_list">
                        <div class="carousel-wrap">
                          <div class="owl-carousel">
                         
                          @foreach($FeaturedProducts1 as $key=>$img)
                         
                            <div class="item">
                             
                              <a  class="item-description">
                                <div class="offer">
                                <span>20% </span>
                              off</div>
                               <form action="{{url('addcart')}}" method="POST">
                              {{csrf_field()}}
                               <div class="item_img"><img src="{{ $img->images[0]->url  ?? asset('assets/user/img/noimage.jpg') }}"></div>
                               <h2>{{  $img->name ?? 'PRODUCT' }}</h2>
                               <div class="rate">
                                <input type="hidden" value="{{$img->shop_id}}" name="shop_id" >
                                <input type="hidden" value="{{$img->id}}" name="product_id" >
                                 <input type="hidden" value="1" name="quantity">
                                  <h3>
                                    <div class="offer-rate"><span>&#x20b9</span>{{ @$img->prices->orignal_price}} </div>
                                    <div class="current-rate"><span>&#x20b9</span>{{ @$img->prices->price}}</div>

                                  </h3>
                                  <div class="quantity">2Kg</div>
                               </div>
                                
                               <div class="add-to-cart">
                                 @if(Auth::user())

                                 <div><button class="add-cart" type="submit"> Add to Cart </button></div></form>
                                 @else
                                 <div><p>Please Login to Add to Cart</p></div>
                                 @endif
                                 <div class="quantity_detail">
                                   <button type="button" class="count-btn less-item">-</button>
                                   <input type="text" class="count_cart" />
                                   <button type="button" class="count-btn add-items">+</button>
                                 </div>
                               </div>
                               </a>
                            </div>
                           @endforeach 
                          
                            
                          

                          </div>
                        </div>
                      </div>
                   </div>


                   <div class="product-items">
                      <h2></h2>
                      <div class="p_item_list">
                        <div class="carousel-wrap">
                          <div class="owl-carousel">
                            @foreach($FeaturedProducts2 as $key=>$img)
                            <div class="item">
                              <a href="#" class="item-description">
                                <div class="offer">
                                <span>20% </span>
                              off</div>
                              <form action="{{url('addcart')}}" method="POST">
                              {{csrf_field()}}
                               <div class="item_img"><img src="{{ $img->images[0]->url  ?? asset('assets/user/img/noimage.jpg') }}"></div>
                               <h2>Tomato Local - Grade A/Naattu Thakkali</h2>
                               <div class="rate">
                                <input type="hidden" value="{{$img->shop_id}}" name="shop_id" >
                                <input type="hidden" value="{{$img->id}}" name="product_id" >
                                 <input type="hidden" value="1" name="quantity">
                                  <h3>
                                    <div class="offer-rate"><span>&#x20b9</span>{{ @$img->prices->orignal_price}} </div>
                                    <div class="current-rate"><span>&#x20b9</span>{{ @$img->prices->price}}</div>

                                  </h3>
                                  <div class="quantity">2Kg</div>
                               </div>
                               <div class="add-to-cart">
                                 @if(Auth::user())

                                 <div><button class="add-cart" type="submit"> Add to Cart </button></div></form>
                                 @else
                                 <div><p>Please Login to Add to Cart</p></div>
                                 @endif
                                 <div class="quantity_detail">
                                   <button type="button" class="count-btn less-item">-</button>
                                   <input type="text" class="count_cart" />
                                   <button type="button" class="count-btn add-items">+</button>
                                 </div>
                               </div>
                               </a>
                              </div>
                              @endforeach
                           
                           
                            

                          </div>
                        </div>
                      </div>
                   </div>

                   <div class="product-items">
                      <h2></h2>
                      <div class="p_item_list">
                        <div class="carousel-wrap">
                          <div class="owl-carousel">
                            @foreach($FeaturedProducts3 as $key=>$img)
                            <div class="item">
                              <a href="#" class="item-description">
                                <div class="offer">
                                <span>20% </span>
                              off</div>
                              <form action="{{url('addcart')}}" method="POST">
                              {{csrf_field()}}
                               <div class="item_img"><img src="{{ $img->images[0]->url  ?? asset('assets/user/img/noimage.jpg') }}"></div>
                               <h2>Tomato Local - Grade A/Naattu Thakkali</h2>
                               <div class="rate">
                                <input type="hidden" value="{{$img->shop_id}}" name="shop_id" >
                                <input type="hidden" value="{{$img->id}}" name="product_id" >
                                 <input type="hidden" value="1" name="quantity">
                                  <h3>
                                    <div class="offer-rate"><span>&#x20b9</span>{{ @$img->prices->orignal_price}} </div>
                                    <div class="current-rate"><span>&#x20b9</span>{{ @$img->prices->price}}</div>

                                  </h3>
                                  <div class="quantity">2Kg</div>
                               </div>
                               <div class="add-to-cart">
                                  @if(Auth::user())

                                 <div><button class="add-cart" type="submit"> Add to Cart </button></div></form>
                                 @else
                                 <div><p>Please Login to Add to Cart</p></div>
                                 @endif
                                 <div class="quantity_detail">
                                   <button type="button" class="count-btn less-item">-</button>
                                   <input type="text" class="count_cart" />
                                   <button type="button" class="count-btn add-items">+</button>
                                 </div>
                               </div>
                               </a>
                              </div>
                              @endforeach
                           
                           
                            

                          </div>
                        </div>
                      </div>
                   </div>
                   @else
                   <div><h5>No Products Available For this Category</h5></div>
                   @endif
                   <!-----------------------closebanner-------------------------------------->
                    <!-----------------------vegetable-------------------------------------->
                  
                   <!-----------------------closevegetable-------------------------------------->
                    <!-----------------------summer drinks-------------------------------------->
                  
                   <!-----------------------closevegetable-------------------------------------->
                   <div class="pantry product-items">
                     <h2>Fill Up Your Pantry</h2>
                     <div class="row">
                       <div class="col-md-6 no-padding left-side">
                         <div class="pantry_product">
                               <a href="#" class="item-description">
                                <div class="offer">
                                <span>20% </span>
                              off</div>
                               <div class="item_img"><img src="{{asset('assets/user/images/namkeen.jpg')}}"></div>
                               <h2>Namkeens</h2>
                             </a>
                         </div>
                       </div>
                       <div class="col-md-6 no-padding pantry-right">
                         <div class="col-sm-6">
                           <div class="pantry_product">
                               <a href="#" class="item-description">
                                <div class="offer">
                                <span>20% </span>
                              off</div>
                               <div class="item_img"><img src="{{asset('assets/user/images/cookie.jpeg')}}"></div>
                               <h2>Biscuits</h2>
                             </a>
                         </div>
                         </div>
                         <div class="col-sm-6">
                           <div class="pantry_product">
                               <a href="#" class="item-description">
                                <div class="offer">
                                <span>20% </span>
                              off</div>
                               <div class="item_img"><img src="{{asset('assets/user/images/chips.png')}}"></div>
                               <h2>Chips & Nachos</h2>
                             </a>
                         </div>
                         </div>
                         <div class="col-sm-6">
                           <div class="pantry_product">
                               <a href="#" class="item-description">
                                <div class="offer">
                                <span>20% </span>
                              off</div>
                               <div class="item_img"><img src="{{asset('assets/user/images/noodles.png')}}"></div>
                               <h2>RTC, Noodles & pasta</h2>
                             </a>
                         </div>
                         </div>
                         <div class="col-sm-6">
                           <div class="pantry_product">
                               <a href="#" class="item-description">
                                <div class="offer">
                                <span>20% </span>
                              off</div>
                               <div class="item_img"><img src="{{asset('assets/user/images/breakfast.jpeg')}}"></div>
                               <h2>Breakfast Cereals</h2>
                             </a>
                         </div>
                         </div>
                       </div>
                     </div>
                   </div>
                    <!-----------------------beauty-------------------------------------->
                  
                </div>
            </div>
        </div>
    </section>

        <!-- Login Content Wrap Ends -->
        <!-- Footer Starts -->
       @include('user.layouts.partials.footer')
        <!-- Footer Ends -->
    </div>
    <!-- Login Warapper Ends -->

    <script src="{{ asset('assets/user/js/jquery.min.js')}}"></script>
    <!-- Bootstrap JS -->
    <script src="{{ asset('assets/user/js/bootstrap.min.js')}}"></script>
    <!-- Slick Slider JS -->
    <script src="{{ asset('assets/user/js/slick.min.js')}}"></script>
    <!-- Sidebar JS -->
    <script src="{{ asset('assets/user/js/asidebar.jquery.js')}}"></script>
    <!-- Map JS -->
    <script src="https://maps.googleapis.com/maps/api/js?key={{Setting::get('GOOGLE_API_KEY')}}></script>
    <script src="{{ asset('assets/user/js/jquery.googlemap.js')}}"></script>
    <!-- Incrementing JS -->
    <script src="{{ asset('assets/user/js/incrementing.js')}}"></script>
    <!-- Scripts -->
    <script src="{{ asset('assets/user/js/owl.carousel.min.js')}}"></script>
    <script src="{{ asset('assets/user/js/scripts.js')}}"></script>
    @include('user.layouts.partials.script')
    <!-- Start of LiveChat (www.livechatinc.com) code -->
    @if(Setting::get('DEMO_MODE') == 0)

         <script type="text/javascript">
              window.__lc = window.__lc || {};
              window.__lc.license = 8256261;
              (function() {
                  var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
                  lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
                  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
              })();
          </script>
           @endif

          <!-- End of LiveChat code -->

    <!-- Global site tag (gtag.js) - Google Analytics -->
             <!--  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-113150309-2"></script>
              <script>
                window.dataLayer = window.dataLayer || [];
                function gtag(){dataLayer.push(arguments);}
                gtag('js', new Date());

                gtag('config', 'UA-113150309-2');
             </script>   -->
<script type="text/javascript">
    $(document).bind("contextmenu",function(e) {
 e.preventDefault();
});
$(document).keydown(function(e){
    console.log(e);
    if(e.which === 123){
       return false;
    }
    if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)) {
     return false;
    }
    if(e.ctrlKey && e.shiftKey && e.keyCode == 'C'.charCodeAt(0)) {
     return false;
    }
    if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)) {
     return false;
    }
    if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)) {
     return false;
    }
});
</script>
<script>
$(document).ready(function()
{

$('#zipcode').change(function(){
    $("#branch").empty();
   var zipcode = $(this).val();

   $.ajax({url: "{{ url('get/branch') }}/"+zipcode,dataType: "json",success: function(data){
        $(data).each(function(index, el) {
             $("#branch").append("<option value="+el.id+">"+el.name+" Branch</option>");
        });
   }});

});
</script>
<script type="text/javascript">
    $('li.mycart_list').click(function()
    {
        $('.cart_list').slideToggle();
    });
    $('li.sub_menu a i').click(function()
      {
        $('li.sub_menu i').toggleClass('open');
        if($('li.sub_menu i').hasClass('open'))
        {
          $('ul.sub_items').slideDown();
        }
        else
        {
          $('ul.sub_items').slideUp();
        }
      });


</script>
<script type="text/javascript">
  var $input =$('.count_cart')
  $input.val(0);
  $('.quantity_detail button').click(function()
    {
      if($(this).hasClass('add-items'))
      {

         $input.val(parseFloat($input.val())+1);
      }
      if($(this).hasClass('less-item'))
      {
        $input.val(parseFloat($input.val())-1);
      }
    });

</script>
<script type="text/javascript">
  $(document).ready(function()
    {
      $('.responsive_humble_menu').click(function()
        {
          $('.side_nav').toggleClass('open-menu');
          if($('.side_nav').hasClass('open-menu'))
          {
            $('body').css('overflow','hidden');
          }
          else
          {
            $('body').css('overflow','scroll');
          }
        });
      $('.right_side_responsive_menu').click(function()
        {
          $('.right_menu').slideToggle();
        });
    });
</script>

<script type="text/javascript">
  $('.owl-carousel').owlCarousel({
  loop: true,
  margin: 10,
  nav: true,
  navText: [
    "<i class='fa fa-caret-left'></i>",
    "<i class='fa fa-caret-right'></i>"
  ],
  autoplay: false,
  autoplayHoverPause: true,
  responsive: {
    0: {
      items: 1
    },
    600: {
      items: 2
    },
    1000: {
      items: 4
    }
  }
})
</script>
<div id="location" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <p>Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
  <div class="modal fade" id="mycart" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
          <p>Some text in the modal.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>
</body>

</html>
