<?php

use Illuminate\Database\Seeder;
use App\Product;
class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->truncate();
        DB::table('products')->insert([
            [
                'name' => 'Fisher Chopped Walnuts',
                'shop_id' => 1,
                'description' => 'special',                
                'featured' => 1,
                'featured_position' => 1
            ],
            [
                'name' => 'Coconut Flakes',
                'shop_id' => 1,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0               
            ],
            [
                'name' => 'Bonito Flakes',
                'shop_id' => 1,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Penne Rigate',
                'shop_id' => 1,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Macaroni and Cheese',
                'shop_id' => 1,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Sweet Peas',
                'shop_id' => 1,
                'description' => 'special',
                'featured' => 1,
                'featured_position' => 2
            ],
            [
                'name' => 'Salad',
                'shop_id' => 1,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Starwberries',
                'shop_id' => 1,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Clementies',
                'shop_id' => 1,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Blueberries',
                'shop_id' => 1,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Deluxe Vividly Vanilla Ice Cream',
                'shop_id' => 2,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Blueberry Waffles',
                'shop_id' => 2,
                'description' => 'special',
                'featured' => 1,
                'featured_position' => 1
            ],
            [
                'name' => 'Bacon Eggs Potatoes & Cheddar Cheese',
                'shop_id' => 2,
                'description' => 'Bacon Eggs Potatoes & Cheddar Cheese',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Pink Frosted Sugar Cookies',
                'shop_id' => 2,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Fresh Goodness Mini M&M Cookies',
                'shop_id' => 2,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Fresh Goodness Birthday Cake Frosted Sugar Cookies',
                'shop_id' => 2,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Fresh Goodness Decorated White Sheet Cake',
                'shop_id' => 2,
                'description' => 'special',
                'featured' => 1,
                'featured_position' => 1
            ],
            [
                'name' => 'Granite Gold Daily Cleaner',
                'shop_id' => 2,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Glade Solid Clean Linen Air Freshener',
                'shop_id' => 2,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Scrubbing Bubbles Daily Shower Cleaner Trigger',
                'shop_id' => 2,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
             [
                'name' => 'LEGO Police Patrol Car',
                'shop_id' => 3,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => ' Build Bonanza',
                'shop_id' => 3,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Breakout Beast Series 1 Eggs',
                'shop_id' => 3,
                'description' => 'special',
                'featured' => 1,
                'featured_position' => 1
            ],
            [
                'name' => 'Budweiser 1933 Repeal Reserve Beer 12 oz Bottles',
                'shop_id' => 3,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Chocolate Syrup',
                'shop_id' => 3,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Classic Roast Medium Ground Coffee',
                'shop_id' => 3,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Jumex Mango Nectar',
                'shop_id' => 3,
                'description' => 'special',
                'featured' => 1,
                'featured_position' => 1

            ],
            [
                'name' => 'Dove Advanced Care Cool ',
                'shop_id' => 3,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
             [
                'name' => 'Garnier Fructis Sleek & Shine Cream',
                'shop_id' => 3,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Premier Protein Shake, Chocolate',
                'shop_id' => 3,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => ' Fat Free Tuna Salad Kit',
                'shop_id' => 4,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Cream of Chicken Condensed Soup',
                'shop_id' => 4,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Light Ranch Dressing ',
                'shop_id' => 4,
                'description' => 'special',
                'featured' => 1,
                'featured_position' => 1
            ],
            [
                'name' => 'Light String Cheese ',
                'shop_id' => 4,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => ' Milk American Singles',
                'shop_id' => 4,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => ' Neufchâtel Cheese',
                'shop_id' => 4,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => ' Fresh Whole Chicken',
                'shop_id' => 4,
                'description' => 'special',
                'featured' => 1,
                'featured_position' => 1
            ],
            [
                'name' => ' Boneless Skinless Chicken Breasts',
                'shop_id' => 4,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
             [
                'name' => ' Fresh Chicken Breasts Tenderloins',
                'shop_id' => 4,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => ' Mild Italian Chicken Sausage',
                'shop_id' => 4,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Debi Lilly Fragrant Rose Bouquet',
                'shop_id' => 5,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Roses Red ',
                'shop_id' => 5,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Tulip Bouquet',
                'shop_id' => 5,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'LaCroix Sparkling Water Grapefruit Cans',
                'shop_id' => 5,
                'description' => 'special',
                'featured' => 1,
                'featured_position' => 1
            ],
            [
                'name' => 'Coca-Cola Soda ',
                'shop_id' => 5,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Cheerios Cereal Toasted Whole Grain Oat ',
                'shop_id' => 5,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'belVita Breakfast Biscuits Blueberry',                
				'shop_id' => 5,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Quaker Oats Old Fashioned',
                'shop_id' => 5,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
             [
                'name' => 'Cheerios Cereal Multi Grain Lightly Sweetened Family Size Box ',
                'shop_id' => 5,
                'description' => 'special',
                'featured' => 1,
                'featured_position' => 1
            ],
            [
                'name' => 'Pop-Tarts Toaster Pastries',
                'shop_id' => 5,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            
        ]);
        
    }
}
