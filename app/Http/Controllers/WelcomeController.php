<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Shop;
use App\EnquiryTransporter;
use Auth;
use App\User;
use App\Order;
use App\Product;
use App\Category;
use App\newsletter;
use Session;
use App\Http\Controllers\Resource\ShopResource;
use App\Http\Controllers\UserResource\CartResource;
class WelcomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    //public function __construct()
    //{
        //$this->middleware('auth');
   // }

    

     /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function home(Request $request)
    { 
       
       //dd($request->all());
        /*$Shop = Shop::take(4)->get();
        $shop_total = Shop::count();
        $user_total = User::count();
        $order_total = Order::where('status','COMPLETED')->count();
        $branches = Shop::wherebranch(1)->get();*/

        $Shop = Shop::wherebranch(0)->first();
        $cat = Category::whereparent_id(0)->get();
        if($request->cat){
           $FeaturedProduct1 = Product::with('images','featured_images','categories','prices')->wherecategory_id($request->cat)->where('shop_id',$Shop->id);
        }else{
           $FeaturedProduct1 = Product::with('images','featured_images','categories','prices')->where('shop_id',$Shop->id); 
        }
        
               /* if($request->has('prodname')){
                    $FeaturedProduct1->where('products.name', 'LIKE', '%' . $request->prodname . '%');
                } */
                if($request->zipcode){
                    $zipcode = $request->zipcode;
                }else{
                    $zipcode = "chennai";
                }
               
        $FeaturedProduct = $FeaturedProduct1->orderBy('featured','ASC')->get();
        //dd($FeaturedProduct);
        $ff = count($FeaturedProduct);
        @$section = $ff/3;
        @$section1S = @$FeaturedProduct->first();
        @$sec1s = @$section1S->id;
        @$sec1e = @$sec1s + @$section ;
        @$sec2s = @$sec1e + 1;
        @$sec2e = @$sec2s + @$section;
        @$sec3s = @$sec2e + 1;
        @$sec3e = @$sec3s + @$section;
        $FeaturedProducts1 = $FeaturedProduct->where('id','>=',@$sec1s)->where('id','<=',@$sec1e);
        $FeaturedProducts2 = $FeaturedProduct->where('id','>=',@$sec2s)->where('id','<=',@$sec2e);
        $FeaturedProducts3 = $FeaturedProduct->where('id','>=',@$sec3s)->where('id','<=',@$sec3e);
        //dd($FeaturedProducts1);
        

        

     
        if(Auth::user()){
           
            $Cart = (new CartResource)->index($request);
                 //dd(count($Cart['carts']));
        }
        return view('user.shop.show',compact('Shop','FeaturedProduct','Cart','cat','zipcode','FeaturedProducts1','FeaturedProducts2','FeaturedProducts3'));
        //dd($Shop);
       // return view('welcome',compact('Shop','shop_total','user_total','order_total','branches'));
    }

    public function branch_list($zipcode)
    {
         $branch =  Shop::wherebranch(1)->wherezipcode($zipcode)->get();
         return $branch;
    }

     /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function faq(Request $request)
    {   
        if($request->ajax()){
            $static = 1;
        }
       return view('faq',compact('static'));
    }
     /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function aboutus()
    {   
       return view('aboutus');
    }
     /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function termcondition(Request $request)
    {   
        $static = 0;
        if($request->has('static')){
            $static = 1;
        }
       return view('term_condition',compact('static'));
    }

    public function newsletter(Request $request)
    {  

        
             $this->validate($request, [
               
                'email_newsletter_2' => 'required|email',
               
             ]);
         try {
             
              newsletter::create([
                'email' => $request->email_newsletter_2
              ]);
            return back()->with('flash_success',trans('home.delivery_boy.created'));

         } catch (Exception $e) {

              return back()->with('flash_error',trans('form.whoops'));
         }

    }
    public function search(Request $request){
        $Shops = [];
        $url = url()->previous();
        $url_segment = explode('/', $url);
        
        if($request->segment(1)!=$url_segment[3]){
            Session::put('search_return_url',$url);
        }
        if($request->has('q')){
            $request->merge(['prodname'=> $request->q]);
            //dd($request->all());
            $shops = (new ShopResource)->filter($request);
            $shops->map(function ($shops) {
                $shops['shopstatus'] = (new ShopResource)->shoptime($shops);;
                $shops['shopopenstatus'] = (new ShopResource)->shoptiming($shops);;
                return $shops;
            });
            //dd($shops);
            foreach($shops as $val){
                if (preg_match("/".$request->q."/i", $val->name, $matches)) {
                    $Shops[$val->id] = $val;
                }else{
                    foreach($val->categories as $valcat){
                        if(count($valcat->products)>0){
                            $Shops[$val->id] = $val;
                        }
                    }
                }
            }
            //dd($Shops);
        }

        return view('search',compact('Shops'));
    }
}
