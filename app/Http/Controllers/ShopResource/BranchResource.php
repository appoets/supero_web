<?php

namespace App\Http\Controllers\ShopResource;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use auth;
use Route;
use Exception;
use App\Shop;
use App\Branch;
use App\ShopTiming;
use App\ShopBanner;
class BranchResource extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $branch = Shop::where('branch',1)->get();
        if($request->ajax()){
            return $branch;
        }
        return view('shop.branch.index',compact('branch'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $Days = [
            'ALL' => 'Everyday',
            'SUN' => 'Sunday',
            'MON' => 'Monday',
            'TUE' => 'Tuesday',
            'WED' => 'Wednesday',
            'THU' => 'Thursday',
            'FRI' => 'Friday',
            'SAT' => 'Saturday'
        ];
        return view('shop.branch.create',compact('Days'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $this->validate($request, [
                'avatar' => 'image|max:5120',
            ]);
        try {
            $shop_id = Auth::guard('shop')->user()->id;
            $branch = $request->all();
            $branch['password'] = bcrypt($request->password);
            $branch['branch'] = 1;
            /*$branch['shop_id'] = $shop_id;*/
            $branch['avatar'] = asset('storage/'.$request->avatar->store('shops/banner'));
            $branch  =  Shop::create($branch);
            if($request->ajax()){
              return $branch;
            }
            //$branch  =  Branch::create($branch);
            return back()->with('flash_success', 'Branch Added Successfully');
        } catch (ModelNotFoundException $e) {
            return back()->with('flash_error', trans('form.not_found'));
        } catch (Exception $e) {
            return back()->with('flash_error', trans('form.whoops'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Days = [
            'ALL' => 'Everyday',
            'SUN' => 'Sunday',
            'MON' => 'Monday',
            'TUE' => 'Tuesday',
            'WED' => 'Wednesday',
            'THU' => 'Thursday',
            'FRI' => 'Friday',
            'SAT' => 'Saturday'
        ];
        try {
            $Shop = Shop::findOrFail($id);
            return view(Route::currentRouteName(), compact('Shop','Days'));
        } catch (ModelNotFoundException $e) {
            // return redirect()->route('admin.shops.index')->with('flash_error', 'Shop not found!');
            return back()->with('flash_error', 'Shop not found!');
        } catch (Exception $e) {
            // return redirect()->route('admin.shops.index')->with('flash_error', trans('form.whoops'));
            return back()->with('flash_error', trans('form.whoops'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255',
                'phone' => 'required|string|max:255',
                // 'cuisine_id' => 'required|array',
                'day' => 'required|array',
                /*'hours_opening.*' => 'date_format:H:i',
                'hours_closing.*' => 'date_format:H:i',*/
                'latitude' => 'required|string|max:255',
                'longitude' => 'required|string|max:255',
                'phone' => 'required|numeric',
                'maps_address' => 'required|string|max:255',
                'address' => 'required|string|max:255',
                'avatar' => 'image|max:2120',
            ]);
/*dd($request->all());*/
        try {
            $Shop = Shop::findOrFail($id);

            $Update = $request->all();
            if($request->hasFile('avatar')) {
                $Update['avatar'] = asset('storage/'.$request->avatar->store('shops'));
            } else {
                unset($Update['avatar']);
            }
            if($request->hasFile('default_banner')) {
                $Update['default_banner'] = asset('storage/'.$request->default_banner->store('shops'));
            } else {
                unset($Update['default_banner']);
            }
            if($request->has('password')) {
                $Update['password'] = bcrypt($request->password);
            } else {
                unset($Update['password']);
            }
            if($request->has('pure_veg')) {
                $Update['pure_veg'] = $request->pure_veg == 'no'?0:1;
            }
            if($request->has('popular')) {
                $Update['popular'] = $request->popular == 'no'?0:1;
            }
            if($request->has('rating_status')) {
                $Update['rating_status'] = 1;
            }else{
                $Update['rating_status'] = 0;
            }
            
            $Shop->update($Update);

            //Cuisine
            // $Shop->cuisines()->detach();
            // if(count($request->cuisine_id)>0) {
            //     foreach($request->cuisine_id as $cuisionk){
            //         $Shop->cuisines()->attach($cuisionk);
            //     }
            // }
            //ShopTimings
            if($request->has('day')) {
                $start_time = $request->hours_opening;
                $end_time = $request->hours_closing;
                ShopTiming::where('shop_id',$id)->delete();
                foreach($request->day as $key => $day) 
                {  
                    $timing[] = [
                        'start_time' => $start_time[$day],
                        'end_time' => $end_time[$day],
                        'shop_id' => $Shop->id,
                        'day' => $day
                        ];
                }
                ShopTiming::insert($timing);   
            }

    
            if($request->status!='active') {
                ShopBanner::where('shop_id',$id)->update(['status'=>'inactive']);
            }

            if($request->ajax()) {
                return $Shop;
            }

            
            return back()->with('flash_success', 'Branch Updated Successfully');
        } catch (ModelNotFoundException $e) {
            return back()->with('flash_error', trans('form.not_found'));
        } catch (Exception $e) {
            return back()->with('flash_error', trans('form.whoops'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $Product = Shop::findOrFail($id);
            // Need to delete variants or have them re-assigned
            $Product->delete();
            if($request->ajax()) {
                return response()->json(['message' => 'Deleted Successfully']); 
            }
            return back()->with('flash_success', "Deleted Successfully");
        } catch (ModelNotFoundException $e) {
            return back()->with('flash_error', 'data not found!');
        } catch (Exception $e) {
            return back()->with('flash_error', trans('form.whoops'));
        }
    }
}
