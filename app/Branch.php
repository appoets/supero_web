<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
   protected $fillable = [
        'name',
        'email',
        'password',
        'phone',
        'avatar',
        'description',
        'offer_min_amount',
        'offer_percent',
        'estimated_delivery_time',
        'address',
        'maps_address',
        'latitude',
        'longitude',
        'pure_veg',
        'status',
        'default_banner',
        'rating',
        'rating_status',
        'popular',
        'device_type',
        'device_token',
        'device_id',
        'otp'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
