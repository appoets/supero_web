<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PrimaryCategory extends Model
{
    protected $fillable = [
        'category',
        'category_product_count'
    ];
}
